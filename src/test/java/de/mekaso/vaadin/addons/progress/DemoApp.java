package de.mekaso.vaadin.addons.progress;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

@Push(PushMode.MANUAL)
@Theme(themeClass = Lumo.class, variant = "dark")
public class DemoApp implements AppShellConfigurator {
	private static final long serialVersionUID = 1L;

}
