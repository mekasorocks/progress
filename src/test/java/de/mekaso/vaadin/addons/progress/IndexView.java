package de.mekaso.vaadin.addons.progress;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.LongStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexDirection;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import de.mekaso.vaadin.addons.progress.builder.ProgressFluidMeterBuilder;

@Route("")
public class IndexView extends VerticalLayout {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(IndexView.class);
	
	public IndexView() {
//		ProgressCircle progressCircle = ProgressCircleBuilder.circular().build();
//		Button starter = new Button("Start", click -> {
//			progressCircle.setValue(0);
//			backendWork(progressCircle);
//		});
//		add(new HorizontalLayout(progressCircle, starter));
		
//		ProgressBarWithSteps pbws = ProgressBarWithStepsBuilder.barWithSteps()
//				.withInitialValue(0)
//				.withStep("get up")
//				.withStep("prepare for work")
//				.withStep("work")
//				.withStep("drink beer")
//				.withStep("go home")
//				.withStep("eat")
//				.withStep("watch TV")
//				.withStep("go to sleep")
//				.build();
//		Button starter = new Button("Start", click -> {
//			backendWork(pbws, click.getSource().getUI().get());
//		});
//		add(new HorizontalLayout(pbws, starter));
		
//		ProgressInfiniteBuilder builder = ProgressComponentBuilder.infinite();
//		addInfiniteProgressComponent("Spinner", builder.withType(InfinityType.Spinner).build(), 12f);
//		addInfiniteProgressComponent("Squid Game", builder.withType(InfinityType.SquidGame).build(), 12f);
//		addInfiniteProgressComponent("Ring", builder.withType(InfinityType.Ring).build(), 12f);
//		addInfiniteProgressComponent("Roller", builder.withType(InfinityType.Roller).build(), 12f);
//		addInfiniteProgressComponent("Circle", builder.withType(InfinityType.Circle).build(), 25f);
//		addInfiniteProgressComponent("Ripple", builder.withType(InfinityType.Ripple).build(), 25f);
//		addInfiniteProgressComponent("Dual Ring", builder.withType(InfinityType.DualRing).build(), 25f);
//		addInfiniteProgressComponent("Glowing Text", ProgressComponentBuilder.infinite().glowingText().build("Mekaso...", "Rules"), 25f);
		
		ProgressFluidMeter fluidMeter = ProgressFluidMeterBuilder.fluid().build();
//		fluidMeter.setWidth(500f, Unit.PIXELS);
//		fluidMeter.setHeight(500f, Unit.PIXELS);
		Button starter = new Button("Start", click -> {
			fluidMeter.setValue(0);
			backendWork(fluidMeter);
		});
		add(new HorizontalLayout(fluidMeter, starter));
	}
	
	private void addInfiniteProgressComponent(String title, InfiniteProgress ip, float height) {
		ProgressControllerButtons buttons = new ProgressControllerButtons();
		FlexLayout fl = new FlexLayout(new H3(title + " (infinite)"), ip, buttons);
		fl.setHeight(33f, Unit.PERCENTAGE);
		fl.setJustifyContentMode(JustifyContentMode.BETWEEN);
		buttons.getResetButton().setText("Stop");
		buttons.getResetButton().setEnabled(true);
		buttons.getResetButton().addClickListener(click -> {
			fl.remove(ip);
			buttons.getStartButton().setEnabled(true);
		});
		buttons.getStartButton().setEnabled(false);
		buttons.getStartButton().addClickListener(click -> {
			fl.addComponentAtIndex(1, ip);
			buttons.getStartButton().setEnabled(false);
			buttons.getResetButton().setEnabled(true);
		});
		buttons.getCodeButton().addClickListener(click -> {
			
		});
		fl.setFlexDirection(FlexDirection.COLUMN);
		fl.setWidth(15f, Unit.PERCENTAGE);
		fl.setAlignItems(Alignment.CENTER);
		add(fl);
	}
	
	private void backendWork(ProgressBarWithSteps pbws, UI ui) {
		long[] waitingTimes = randomSteps(pbws.getSteps().size(), 1000, 2500).toArray();
		logger.debug("starting {} steps.", waitingTimes.length);
		Long waitingTime = Long.valueOf(0);
		for (AtomicInteger counter = new AtomicInteger(0); counter.get() < waitingTimes.length; counter.incrementAndGet()) {
			long time = waitingTimes[counter.get()];
			waitingTime = Math.addExact(waitingTime, time);
			doTask(waitingTime, counter.get())
			.whenComplete((currentStep, exception) -> {
				if (exception == null) {
					onStepDone(currentStep, pbws, ui);
				}
			});
		}
	}
	
	private void onStepDone(int stepNumber, ProgressBarWithSteps pbws, UI ui) {
		ui.access(() -> {
			logger.debug("step {} '{}' is finished.", stepNumber, pbws.getSteps().get(stepNumber));
			pbws.setCurrentStepNumber(stepNumber + 1);
			ui.push();
		});
    }

	private void backendWork(ProgressComponent circularProgress) {
		long[] waitingTimes = randomSteps(20, 500, 1200).toArray();
		long sum = LongStream.of(waitingTimes).sum();
		AtomicLong waitingSum = new AtomicLong();
		for (int counter = 0; counter < waitingTimes.length; counter++) {
			long waitingTime = waitingTimes[counter];
			waitingSum.addAndGet(waitingTime);
			int value = Math.round((waitingSum.get() * 100) / sum);
			doTask(waitingSum.get(), value)
			.whenComplete((progressValue, exception) -> {
				circularProgress.getUI().ifPresent(ui -> {
					ui.access(() -> {
						circularProgress.setValue(progressValue.intValue());
						ui.push();
					});
				});
			});
		}
	}
	
	private CompletableFuture<Integer> doTask(long waitingTime, Integer stepNumber) {
		CompletableFuture<Integer> cf = new CompletableFuture<>();
		return cf.completeAsync(() -> {
//            logger.debug("sleeping {} ms for step {}.", waitingTime, stepNumber);
			try {
				TimeUnit.MILLISECONDS.sleep(waitingTime);
			} catch (InterruptedException e) {
				throw new CompletionException(e);
			}
			return stepNumber;
		});
    }
	
	private LongStream randomSteps(int numberOfSteps, int minTime, int maxTime) {
		Random random = new Random();
		return random.longs(numberOfSteps, minTime, maxTime + 1);
	}
}
