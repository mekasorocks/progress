package de.mekaso.vaadin.addons.progress;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class ProgressControllerButtons extends FlexLayout {
	private static final long serialVersionUID = 1L;
	private Button startButton;
	private Button resetButton;
	private Button codeButton;
	
	public ProgressControllerButtons() {
		addClassNames(LumoUtility.JustifyContent.EVENLY, LumoUtility.Gap.MEDIUM, LumoUtility.FlexWrap.WRAP);
		this.startButton = new Button("Start");
		this.resetButton = new Button("Reset");
		this.resetButton.setEnabled(false);
		this.codeButton = new Button("Code");
		add(this.startButton, this.resetButton, this.codeButton);
	}
	
	/**
	 * @return the startButton
	 */
	public Button getStartButton() {
		return startButton;
	}
	/**
	 * @return the resetButton
	 */
	public Button getResetButton() {
		return resetButton;
	}

	/**
	 * @return the codeButton
	 */
	public Button getCodeButton() {
		return codeButton;
	}
}
