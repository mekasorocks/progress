package de.mekaso.vaadin.addons.progress.builder;

import de.mekaso.vaadin.addons.progress.InfiniteProgress;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.Circle;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.DualRing;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.InfinityType;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.Ring;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.Ripple;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.Roller;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.Spinner;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.SquidGame;

public class ProgressInfiniteBuilder {

	private InfinityType type;
	
	public ProgressInfiniteBuilder withType(InfinityType type) {
		this.type = type;
		return this;
	}
	
	public GlowingTextBuilder glowingText() {
		return new GlowingTextBuilder();
	}
	
	
	public InfiniteProgress build() {
		InfiniteProgress ip = null;
		switch (this.type) {
			case Circle:
				ip = new Circle(this.type);
				break;
			case DualRing:
				ip = new DualRing(this.type);
				break;
			case Ring:
				ip = new Ring(this.type);
				break;
			case Ripple:
				ip = new Ripple(this.type);
				break;
			case Roller:
				ip = new Roller(this.type);
				break;
			case Spinner:
				ip = new Spinner(this.type);
				break;
			case SquidGame:
				ip = new SquidGame(this.type);
				break;
			default:
				break;
		}
		return ip;
	}	
}