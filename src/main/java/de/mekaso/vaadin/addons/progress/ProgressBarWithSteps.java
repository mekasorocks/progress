package de.mekaso.vaadin.addons.progress;

import java.util.List;

import com.vaadin.flow.component.PropertyDescriptor;
import com.vaadin.flow.component.PropertyDescriptors;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;

/**
 * A progress bar component.
 * @author Meik Kaufmann
 */
@Tag("progress-bar-with-steps")
@JsModule("./bar/progress-bar-with-steps.ts")
@CssImport("./bar/progress-bar-with-steps.css")
public class ProgressBarWithSteps extends ProgressComponent {

	private static final long serialVersionUID = 1L;
	/**
	 * the work steps.
	 */
	private List<String> steps;
	/**
	 * Vaadin property descriptor for the currentStepNumber property.
	 */
	private static PropertyDescriptor<Integer, Integer> CURRENT_STEP_NUMBER = PropertyDescriptors.propertyWithDefault("currentStepNumber", Integer.valueOf(0));
	/**
	 * the current step number
	 */
	private int currentStepNumber = 0;

	/**
	 * Create a progress bar with steps.
	 * @param currentStepNumber the current step number
	 * @param steps a list of steps (not null)
	 */
	public ProgressBarWithSteps(int currentStepNumber, List<String> steps) {
		super((currentStepNumber * 100) / steps.size());
		this.currentStepNumber = currentStepNumber;
		this.steps = steps;
		getElement().setPropertyList("steps", this.steps);
	}
	
	public void reset() {
		this.currentStepNumber = 0;
		set(CURRENT_STEP_NUMBER, -1);
	}
	
	/**
	 * Get the list of steps.
	 * @return the steps
	 */
	public List<String> getSteps() {
		return steps;
	} 
	
	/**
	 * get the current step number.
	 * @return the currentStepNumber
	 */
	public int getCurrentStepNumber() {
		return this.currentStepNumber;
	}

	/**
	 * set current step number (and calculate the current progress in percent).
	 * @param currentStepNumber the currentStepNumber to set
	 */
	public void setCurrentStepNumber(int currentStepNumber) {
		this.currentStepNumber = currentStepNumber;
		set(CURRENT_STEP_NUMBER, currentStepNumber);
		super.setValue((currentStepNumber * 100) / this.steps.size());
	}
}