package de.mekaso.vaadin.addons.progress;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;

/**
 * a basic circular progress component.
 * @author Meik Kaufmann
 */
@Tag("progress-circle")
@JsModule("./circle/progress-circle.ts")
@CssImport("./circle/progress-circle.css")
public class ProgressCircle extends ProgressComponent {

	private static final long serialVersionUID = 1L;
	
	/**
	 * create a circular progress component.
	 * @param value the progress value
	 */
	public ProgressCircle(int value) {
		super(value);
	}
}