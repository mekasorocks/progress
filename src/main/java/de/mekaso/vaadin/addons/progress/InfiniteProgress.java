package de.mekaso.vaadin.addons.progress;

import java.util.Arrays;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.PropertyDescriptor;
import com.vaadin.flow.component.PropertyDescriptors;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;

/**
 * a component to indicate an infinite process.
 * @author Meik Kaufmann
 */

public abstract class InfiniteProgress extends Component implements HasSize, HasStyle {

	private static final long serialVersionUID = 1L;
	private InfinityType type;
	
	public enum InfinityType {
		SquidGame, Spinner, Circle, GlowingText, Ring, Roller, Ripple, DualRing 
	}
	
	public InfiniteProgress(InfinityType type) {
		this.type = type;
	}
	
	/**
	 * @return the type
	 */
	public InfinityType getType() {
		return type;
	}

	@JsModule("./infinite/SquidGame.ts")
	@Tag("progress-infinite-squid-game")
	public static class SquidGame extends InfiniteProgress {
		private static final long serialVersionUID = 1L;
		public SquidGame(InfinityType type) {
			super(type);
		}
	}
	
	@JsModule("./infinite/Spinner.ts")
	@Tag("progress-infinite-spinner")
	public static class Spinner extends InfiniteProgress {
		private static final long serialVersionUID = 1L;
		public Spinner(InfinityType type) {
			super(type);
		}
	}
	
	@JsModule("./infinite/Circle.ts")
	@Tag("progress-infinite-circle")
	public static class Circle extends InfiniteProgress {
		private static final long serialVersionUID = 1L;
		public Circle(InfinityType type) {
			super(type);
		}
	}
	
	@JsModule("./infinite/GlowingText.ts")
	@Tag("progress-infinite-glowing-text")
	public static class GlowingText extends InfiniteProgress {
		private static final long serialVersionUID = 1L;
		private static PropertyDescriptor<String, String> TOP_TEXT = PropertyDescriptors.propertyWithDefault("topText", "Loading...");
		private static PropertyDescriptor<String, String> BOTTOM_TEXT = PropertyDescriptors.propertyWithDefault("bottomText", "Loading...");
		private static final int MAX_LENGTH = 30;
		
		public GlowingText(InfinityType type) {
			super(type);
		}
		
		public GlowingText withTopText(String text) {
			String result = "";
			if (text.length() >= MAX_LENGTH) {
				result = text.substring(0, MAX_LENGTH - 1);
			} else {
				result = text;
			}
			set(TOP_TEXT, result);
			return this;
		}
		public GlowingText withBottomText(String text) {
			String result = "";
			if (text.length() >= MAX_LENGTH) {
				result = text.substring(0, MAX_LENGTH - 1);
			} else {
				result = text;
			}
			set(BOTTOM_TEXT, result);
			return this;
		}
	}
	
	@JsModule("./infinite/Ring.ts")
	@Tag("progress-infinite-ring")
	public static class Ring extends InfiniteProgress {
		private static final long serialVersionUID = 1L;
		public Ring(InfinityType type) {
			super(type);
		}
	}
	
	@JsModule("./infinite/Roller.ts")
	@Tag("progress-infinite-roller")
	public static class Roller extends InfiniteProgress {
		private static final long serialVersionUID = 1L;
		public Roller(InfinityType type) {
			super(type);
		}
	}
	
	@JsModule("./infinite/Ripple.ts")
	@Tag("progress-infinite-ripple")
	public static class Ripple extends InfiniteProgress {
		private static final long serialVersionUID = 1L;
		public Ripple(InfinityType type) {
			super(type);
		}
	}
	
	@JsModule("./infinite/DualRing.ts")
	@Tag("progress-infinite-dual-ring")
	public static class DualRing extends InfiniteProgress {
		private static final long serialVersionUID = 1L;
		public DualRing(InfinityType type) {
			super(type);
		}
	}
}
