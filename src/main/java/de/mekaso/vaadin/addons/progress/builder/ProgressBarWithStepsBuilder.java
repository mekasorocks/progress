package de.mekaso.vaadin.addons.progress.builder;

import java.util.LinkedList;
import java.util.List;

import de.mekaso.vaadin.addons.progress.ProgressBarWithSteps;

/**
 * Builder for a ProgressBarWithSteps component.
 * @author Meik Kaufmann
 */
public class ProgressBarWithStepsBuilder extends ProgressComponentBuilder<ProgressBarWithStepsBuilder> {
	/**
	 * a list of process steps.
	 */
	private List<String> steps = new LinkedList<>();
	/**
	 * constructor.
	 */
	protected ProgressBarWithStepsBuilder() { }
	
	/**
	 * Add a process step.
	 * @param step a process step
	 * @return the builder
	 */
	public ProgressBarWithStepsBuilder withStep(String step) {
		this.steps.add(step);
		return this;
	}
	
	/**
	 * create a ProgressBarWithSteps component.
	 * @return the component
	 */
	public ProgressBarWithSteps build() {
		List<String> mySteps = new LinkedList<>(this.steps);
		ProgressBarWithSteps progressBarWithSteps = new ProgressBarWithSteps(this.initialValue, mySteps);
		if (this.listener != null) {
			progressBarWithSteps.addProgressFinishedListener(this.listener);
		}
		this.steps.clear();
		return progressBarWithSteps;
	}
}
