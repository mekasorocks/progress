package de.mekaso.vaadin.addons.progress.builder;

import de.mekaso.vaadin.addons.progress.ProgressCircle;

/**
 * Builder for a ProgressCircleBuilder component.
 * @author Meik Kaufmann
 */
public class ProgressCircleBuilder extends ProgressComponentBuilder<ProgressCircleBuilder> {
	/**
	 * constructor.
	 */
	protected ProgressCircleBuilder() { }
	
	/**
	 * create a ProgressCircle component.
	 * @return the component
	 */
	public ProgressCircle build() {
		ProgressCircle progressCircle = new ProgressCircle(this.initialValue);
		if (this.listener != null) {
			progressCircle.addProgressFinishedListener(this.listener);
		}
		return progressCircle;
	}
}