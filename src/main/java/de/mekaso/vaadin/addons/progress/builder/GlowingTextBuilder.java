package de.mekaso.vaadin.addons.progress.builder;

import de.mekaso.vaadin.addons.progress.InfiniteProgress;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.GlowingText;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.InfinityType;

public class GlowingTextBuilder {

	public InfiniteProgress build(String topText, String bottomText) {
		return new GlowingText(InfinityType.GlowingText).withTopText(topText).withBottomText(bottomText);
	}
}
