package de.mekaso.vaadin.addons.progress.builder;

import de.mekaso.vaadin.addons.progress.ProgressFluidMeter;

/**
 * Builder for a ProgressFluidMeter component.
 * @author Meik Kaufmann
 */
public class ProgressFluidMeterBuilder extends ProgressComponentBuilder<ProgressFluidMeterBuilder> {
	/**
	 * constructor.
	 */
	protected ProgressFluidMeterBuilder() { }

	/**
	 * create a ProgressFluidMeter component.
	 * @return the component
	 */
	public ProgressFluidMeter build() {
		ProgressFluidMeter progressFluidMeter = new ProgressFluidMeter(this.initialValue);
		if (this.listener != null) {
			progressFluidMeter.addProgressFinishedListener(this.listener);
		}
		return progressFluidMeter;
	}
}