package de.mekaso.vaadin.addons.progress.event;

import com.vaadin.flow.component.ComponentEvent;

import de.mekaso.vaadin.addons.progress.ProgressComponent;

/**
 * Custom event that is fired after the progress is completed. 
 * @author Meik Kaufmann
 *
 */
public class ProgressFinishedEvent extends ComponentEvent<ProgressComponent>{
	private static final long serialVersionUID = 1L;

	/**
	 * The Event constructor.
	 * @param source the component
	 * @param fromClient if the event was fired from client
	 */
	public ProgressFinishedEvent(ProgressComponent source, boolean fromClient) {
		super(source, fromClient);
	}
}