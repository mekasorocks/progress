package de.mekaso.vaadin.addons.progress.builder;

import com.vaadin.flow.component.ComponentEventListener;

import de.mekaso.vaadin.addons.progress.event.ProgressFinishedEvent;

/**
 * Builder for all types of progress component builders.
 * @author Meik Kaufmann
 *
 * @param <T> the type
 */
public abstract class ProgressComponentBuilder<T extends ProgressComponentBuilder<?>> {

	/**
	 * the listener for progress finished events.
	 */
	protected ComponentEventListener<ProgressFinishedEvent> listener;
	/**
	 * 
	 */
	protected int initialValue = 0;
	
	/**
	 * Add a ComponentEventListener for ProgressFinishedEvent events.
	 * @param listener a listener
	 * @return the builder
	 */
	@SuppressWarnings("unchecked")
	public T withProgressFinishedListener(ComponentEventListener<ProgressFinishedEvent> listener) {
		this.listener = listener;
		return (T) this;
	}
	
	/**
	 * @param value the initial value (in %).
	 * @return the builder
	 */
	@SuppressWarnings("unchecked")
	public T withInitialValue(int value) {
		this.initialValue = value;
		return (T) this;
	}
	
	/**
	 * create a builder for ProgressCircle components.
	 * @return the builder
	 */
	public static ProgressCircleBuilder circular() {
		return new ProgressCircleBuilder();
	}
	
	/**
	 * create a builder for ProgressFluidMeter components.
	 * @return the builder
	 */
	public static ProgressFluidMeterBuilder fluid() {
		return new ProgressFluidMeterBuilder();
	}
	
	/**
	 * create a builder for ProgressBarWithSteps components.
	 * @return the builder
	 */
	public static ProgressBarWithStepsBuilder barWithSteps() {
		return new ProgressBarWithStepsBuilder();
	}
	
	/**
	 * create a builder for infinite progress components.
	 * @return the builder
	 */
	public static ProgressInfiniteBuilder infinite() {
		return new ProgressInfiniteBuilder();
	}
}