package de.mekaso.vaadin.addons.progress;

import com.vaadin.flow.component.PropertyDescriptor;
import com.vaadin.flow.component.PropertyDescriptors;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;

/**
 * circular fluid meter component.
 * @author Meik Kaufmann
 */
@Tag("progress-fluid-meter")
@NpmPackage(value = "fluid-meter", version = "1.1.2")
@JsModule("./fluid/ProgressFluidMeter.ts")
public class ProgressFluidMeter extends ProgressComponent {

	private static final long serialVersionUID = 1L;
	private static PropertyDescriptor<String, String> WIDTH = PropertyDescriptors.propertyWithDefault("width", "250px");
	private static PropertyDescriptor<String, String> HEIGHT = PropertyDescriptors.propertyWithDefault("height", "250px");
	
	/**
	 * Create a circular fluid meter component.
	 * @param value the progress value
	 */
	public ProgressFluidMeter(int value) {
		super(value);
		setWidth(250f, Unit.PIXELS);
		setHeight(250f, Unit.PIXELS);
	}
	
	@Override
	public void setWidth(String width) {
		super.setWidth(width);
		set(WIDTH, width);
	}
	
	@Override
	public void setWidth(float width, Unit unit) {
		super.setWidth(width, unit);
		set(WIDTH, width + unit.toString());
	}
	
	@Override
	public void setHeight(String height) {
		super.setHeight(height);
		set(HEIGHT, height);
	}
	
	@Override
	public void setHeight(float height, Unit unit) {
		super.setHeight(height, unit);
		set(HEIGHT, height + unit.toString());
	}
}