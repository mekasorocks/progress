package de.mekaso.vaadin.addons.progress;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.PropertyDescriptor;
import com.vaadin.flow.component.PropertyDescriptors;
import com.vaadin.flow.shared.Registration;

import de.mekaso.vaadin.addons.progress.event.ProgressFinishedEvent;

/**
 * Base class for all progress components.
 * @author Meik Kaufmann
 */
public abstract class ProgressComponent extends Component implements HasSize, HasStyle {
	private static final long serialVersionUID = 1L;
	/**
	 * Vaadin property descriptor for the progressValue property.
	 */
	private static PropertyDescriptor<Integer, Integer> VALUE = PropertyDescriptors.propertyWithDefault("progressValue", Integer.valueOf(0));
	/**
	 * the current progress value.
	 */
	private int value;
	
	/**
	 * Constructor.
	 * @param value the value
	 */
	public ProgressComponent(int value) {
		this.value = value;
	}
	
	/**
	 * add a progress finished listener to the component.
	 * @param listener the listener
	 * @return a registration
	 */
	public Registration addProgressFinishedListener(ComponentEventListener<ProgressFinishedEvent> listener) {
		return addListener(ProgressFinishedEvent.class, listener);
	} 

	/**
	 * get the current progress value in percent.
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * set the progress value (in percent)
	 * @param value the value to set
	 */
	public void setValue(int value) {
		if (value < 0) {
			value = 0;
		} else if (value >= 100) {
			value = 100;
			fireEvent(new ProgressFinishedEvent(this, false));
		}
		this.value = value;
		set(VALUE, value);
	}
}
