import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import '@vaadin/vaadin-lumo-styles/typography';
import '@vaadin/vaadin-lumo-styles/color';

/**
 * 
 */
@customElement('progress-infinite-dual-ring')
class DualRing extends LitElement {
    
    static styles = css`
        .lds-dual-ring {
            display: inline-block;
            width: 80px;
            height: 80px;
        }
        .lds-dual-ring:after {
            content: " ";
            display: block;
            width: 64px;
            height: 64px;
            margin: 8px;
            border-radius: 50%;
            border: 6px solid var(--lumo-primary-color-50pct);
            border-color: var(--lumo-primary-color-50pct) transparent var(--lumo-primary-color-50pct) transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }
        @keyframes lds-dual-ring {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }
    `
    
    render() {
        return html`<div class="lds-dual-ring"></div>`;
    }
}