import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import '@vaadin/vaadin-lumo-styles/color';
/**
 * 
 */
@customElement('progress-infinite-ring')
class Ring extends LitElement {
    
    static styles = css`
        .overlay-container{
            position: relative;
        }
        .box{
            width: 100%;
            height: 100%;            
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0.8;  /* for demo purpose  */
        }
        .stack-top {
            z-index: 9;
            margin: 20px; /* for demo purpose  */
        }
    
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 64px;
            height: 64px;
            margin: 8px;
            border: 8px solid var(--lumo-primary-color-50pct);
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: var(--lumo-primary-color-50pct) transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }
    `
    
    render() {
        return html`<div class="overlay-container">
            <div class="lds-ring box stack-top"><div></div><div></div><div></div><div></div></div>
        </div>`;
    }
}