import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import '@vaadin/vaadin-lumo-styles/color';

/**
 * 
 */
@customElement('progress-infinite-ripple')
class Ripple extends LitElement {
    
    static styles = css`
        .lds-ripple {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }
        .lds-ripple div {
            position: absolute;
            border: 4px solid var(--lumo-primary-color-50pct);
            opacity: 1;
            border-radius: 50%;
            animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
        }
        .lds-ripple div:nth-child(2) {
            animation-delay: -0.5s;
        }
        @keyframes lds-ripple {
          0% { top: 36px; left: 36px; width: 0; height: 0; opacity: 1; }
          100% { top: 0px; left: 0px; width: 72px; height: 72px; opacity: 0; }
        }
    `
    
    render() {
        return html`<div class="lds-ripple"><div></div><div></div></div>`;
    }
}