import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import '@vaadin/vaadin-lumo-styles/color';

/**
 * https://codepen.io/derekmorash/pen/grvOZx.
 */
@customElement('progress-infinite-circle')
class Circle extends LitElement {
    
    static styles = css`
        div {
            width: 8vmax;
            height: 8vmax;
            border-right: 4px solid var(--lumo-primary-color-50pct);
            border-radius: 100%;
            -webkit-animation: spinRight 1500ms linear infinite;
            animation: spinRight 1500ms linear infinite;
        }
        div:before, div:after {
            content: "";
            width: 6vmax;
            height: 6vmax;
            display: block;
            position: absolute;
            top: calc(50% - 3vmax);
            left: calc(50% - 3vmax);
            border-left: 3px solid var(--lumo-primary-color-50pct);
            border-radius: 100%;
            -webkit-animation: spinLeft 1500ms linear infinite;
            animation: spinLeft 1500ms linear infinite;
        }
        div:after {
            width: 4vmax;
            height: 4vmax;
            top: calc(50% - 2vmax);
            left: calc(50% - 2vmax);
            border: 0;
            border-right: 2px solid #ffffff;
            -webkit-animation: none;
            animation: none;
        }
        
        @-webkit-keyframes spinLeft {
            from { transform: rotate(0deg); }
            to { transform: rotate(720deg); }
        }
        @keyframes spinLeft {
            from { transform: rotate(0deg); }
            to { transform: rotate(720deg); }
        }
        @-webkit-keyframes spinRight {
            from { transform: rotate(360deg); }
            to { transform: rotate(0deg); }
        }
        @keyframes spinRight {
            from { transform: rotate(360deg); }
            to { transform: rotate(0deg); }
        }
    `
    
    render() {
        return html`<div></div>`;
    }
}