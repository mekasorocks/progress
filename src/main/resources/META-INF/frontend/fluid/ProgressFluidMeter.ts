import { css, html, LitElement, TemplateResult } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { CircularFluidMeter, CircularFluidMeterConfig, Speed } from 'fluid-meter';
import * as fluid from './fluid-configurations';
import '@vaadin/vaadin-lumo-styles/typography';
import '@vaadin/vaadin-lumo-styles/color';

/**
 * Taken from https://github.com/aarcoraci/fluid-meter
 */
@customElement('progress-fluid-meter')
class ProgressFluidMeter extends LitElement {

	@property()
    progressValue : number;
    @property()
    width : String;
    @property()
    height : String;
    
    @query('div') 
    targetContainer!: HTMLDivElement|null;

    _fluidMeter : CircularFluidMeter | null;
    
    constructor() {
        super();
        this.progressValue = 0;
        this.width = "250px";
        this.height = "250px";
		this._fluidMeter = null;
    }
    
    firstUpdated() : void {
        let fontFamily : string = getComputedStyle(document.documentElement).getPropertyValue('--lumo-font-family');
        let fontSize : string = getComputedStyle(document.documentElement).getPropertyValue('--lumo-size-l');
        let textColor : string = getComputedStyle(document.documentElement).getPropertyValue('--lumo-body-text-color');
        let backgroundColor : string = getComputedStyle(document.documentElement).getPropertyValue('--lumo-base-color');
        let foregroundColor : string = '#e4f0ff'; //getComputedStyle(document.documentElement).getPropertyValue('--lumo-primary-color-10pct');
        let backgroundFillColor : string = getComputedStyle(document.documentElement).getPropertyValue('--lumo-primary-color-50pct');
        let foregroundFillColor : string = getComputedStyle(document.documentElement).getPropertyValue('--lumo-primary-color');
        
        const target = this.targetContainer as HTMLDivElement;
        const config = fluid.configurations[1];
        config.fontFamily = fontFamily;
        //config.textColor = textColor;
        //config.backgroundColor = backgroundColor;
        //config.borderColor = foregroundFillColor;
        //config.bubbleColor = "yellow";
        //console.log(config);
        this._fluidMeter = new CircularFluidMeter(target, config);
    }
    
    disconnectedCallback() :void {
    	super.disconnectedCallback();
    	//if (this._fluidMeter != null) {
	    //	this._fluidMeter.dispose();
	    //}
    }

    static styles = css``

    render() : TemplateResult {
	    if (this._fluidMeter != null) {
	    	this._fluidMeter.progress = this.progressValue;
	    }
		return html`<div style="width:${this.width};height:${this.height}"></div>`;
    }
}