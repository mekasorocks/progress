import { CircularFluidMeterConfig, Speed } from 'fluid-meter';

export const configurations : CircularFluidMeterConfig[] = [
{
	borderWidth: 14,
	initialProgress: 0,
	fontSize: 30,
	use3D: false,
	textDropShadow: false,
	progressFormatter: (value) => {
		return `${value.toFixed(0)} %`;
	},
},
	{
		borderWidth: 16,
		  initialProgress: 0,
		  maxProgress: 100,
		  backgroundColor: '#002d59',
		  borderColor: '#3e4954',
		  bubbleColor: '#6bcfff',
		  fontFamily: 'Codystar',
		  fontSize: 34,
		  progressFormatter: (value) => {
		    return `${value.toFixed(0)} %`;
		  },
		  fluidConfiguration: {
		    color: '#1e90ff'
		  }
	},
	{
  borderColor: '#567656',
  initialProgress: 0,
  fontSize: 90,
  backgroundColor: '#2d3d2d',
  textColor: '#80cd32',
  fontFamily: 'Creepster',
  progressFormatter: (value) => {
    return value > 50 ? value.toFixed(1) + '!' : value.toFixed(1);
  },
  fluidConfiguration: {
    color: '#adff2f',
    horizontalSpeed: Speed.FAST,
    waveSpeed: Speed.FAST
  }
},
{
  initialProgress: 0,
  borderColor: '#2a2a2a',
  backgroundColor: '#270100',
  showBubbles: false,
  borderWidth: 45,
  fontSize: 27,
  progressFormatter: (value) => {
    return value.toFixed(2) + '%';
  },
  fluidConfiguration: {
    color: '#ff4500',
    horizontalSpeed: Speed.SLOW,
    waveSpeed: Speed.SLOW
  }
},
{
  initialProgress: 0,
  maxProgress: 500,
  borderColor: '#dadada',
  backgroundColor: '#dadada',
  showBubbles: true,
  borderWidth: 22,
  dropShadow: false,
  padding: 0,
  fontSize: 34,
  progressFormatter: (value) => {
    return `${value.toFixed(0)} / 500`;
  },
  fluidConfiguration: {
    color: '#800080'
  }
},
{
  borderWidth: 22,
  initialProgress: 0,
  padding: 50,
  use3D: false,
  fontFamily: 'Shizuru',
  backgroundColor: '#002d59',
  borderColor: '#3e4954',
  bubbleColor: '#6bcfff',
  fontSize: 60,
  progressFormatter: (value) => {
    return value.toFixed(1) + '%';
  },
  fluidConfiguration: {
    color: '#1e90ff',
    horizontalSpeed: Speed.FAST,
    waveSpeed: Speed.FAST
  }
},
{
  borderWidth: 22,
  initialProgress: 0,
  padding: 50,
  backgroundColor: '#208000',
  borderColor: '#800060',
  showProgress: false,
  showBubbles: false,
  dropShadow: false,
  use3D: false,
  fluidConfiguration: {
    color: '#f8f8ff'
  }
}
];