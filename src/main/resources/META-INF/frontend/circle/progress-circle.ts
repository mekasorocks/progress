import { css, html, LitElement, TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';

/**
 * Taken from https://codepen.io/equinusocio/pen/OJMBpdK
 */
@customElement('progress-circle')
class ProgressCircle extends LitElement {
    
    @property({ type: Number }) progressValue : number;

    constructor() {
        super();
        this.progressValue = 0;
    }

    render() : TemplateResult {
    	if (!this.progressValue) {
    		this.progressValue = 0;
    	}
        this.style.setProperty('--progress', this.progressValue + "%");
        return html`
        	<span>${this.progressValue} %</span>
        `;
    }
}