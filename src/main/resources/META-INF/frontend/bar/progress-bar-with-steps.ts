import { css, html, LitElement, TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
//import * as styles from './progress-bar-with-steps-styles';
import '@vaadin/icon';
import '@vaadin/icons';
import '@vaadin/vaadin-lumo-styles/typography';
import '@vaadin/vaadin-lumo-styles/color';

/**
 * Taken from https://codepen.io/lazercaveman/pen/ePNbzd
 */
@customElement('progress-bar-with-steps')
class ProgressBarWithSteps extends LitElement {

    @property({ type: Number }) 
    progressValue: number;
    
    @property({ type: Number }) 
    currentStepNumber: number;

    @property({ type: Array<string> }) 
    steps: Array<string>;
    
    static styles = css`
        :host {
            min-width: 320px;
        }
        :host .bar {
          position: relative;
          display: flex;
          align-items: center;
          width: 100%;
          height: 40px;
          background-color: var(--lumo-contrast-10pct);
          border-radius: 40px;
          overflow: hidden;
        }
        :host .bar .img-box {
          position: relative;
          z-index: 1;
          display: flex;
          justify-content: center;
          align-items: center;
          margin-right: 10px;
          width: 40px;
          height: 40px;
          background-color: var(--lumo-base-color);
          border-radius: 50%;
        }
        :host .bar .img-box vaadin-icon {
          color: var(--lumo-body-text-color);
        }
        :host .bar p {
          position: relative;
          z-index: 1;
          color: var(--lumo-body-text-color);
          font-size: var(--lumo-font-size-xl);
          font-family: var(--lumo-font-family);
          left: 20px;
        }
        :host .progress {
          position: absolute;
          top: 0;
          left: 0;
          min-width: 1px;
          height: 100%;
          background: linear-gradient(30deg, var(--lumo-primary-color-10pct), var(--lumo-primary-color));
          border-radius: 20px;
          transition: 1s ease;
        }
        :host .progress.running {
          animation-name: fill;
          animation-delay: 0s;
          animation-duration: 1s;
          animation-iteration-count: "infinite";
          animation-fill-mode: forwards;
        }
        @keyframes fill {
          0% {
            border-radius: 0 20px 20px 0;
          }
          50% {
            border-radius: 0 15px 15px 0;
          }
          100% {
            border-radius: 0 20px 20px 0;
          }
        }
        @media screen and (min-width: 600px) {
          #container .content {
            display: grid;
            grid-gap: 20px;
            grid-template-columns: repeat(2, 1fr);
          }
        }`

    constructor() {
        super();
        this.progressValue = 0;
        this.currentStepNumber = 0;
        this.steps = [];
    }

    render() : TemplateResult {
        if (this.currentStepNumber === -1) {
            this.currentStepNumber = 0;
            this.progressValue = 0.0;
        }
        let numberOfSteps: number = this.steps.length;
        let percent: string = this.progressValue + "%";
        let step : string = this.currentStepNumber === 0 ? '' : this.steps[this.currentStepNumber - 1];
        let progress : TemplateResult = !this.currentStepNumber ? html`<p>0 / ${numberOfSteps}</p>` : html`<p>${this.currentStepNumber} / ${numberOfSteps} (${percent}) ${step}</p>`;
        console.log("Rendering step '" + step + "' " + (this.currentStepNumber) + " of " + numberOfSteps);
        return html`
            <div class="bar">
                <div class="progress" style="width: ${percent}"></div>
                ${progress}
            </div>
        `;
    }
}